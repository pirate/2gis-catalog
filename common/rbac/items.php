<?php
return [
    \common\models\User::ROLE_SERVICE_OWNER => [
        'type' => 1,
        'description' => 'Владелец',
    ],
    \common\models\User::ROLE_ADMINISTRATOR => [
        'type' => 1,
        'description' => 'Администратор',
    ],
];
