<?php

namespace common\rbac;

use common\models\User;
use yii\rbac\Assignment;
use yii\rbac\PhpManager;
use Yii;

/**
 * Class AuthManager
 * @package common\rbac
 */
class AuthManager extends PhpManager
{
    /**
     * @param $userId
     * @return array|mixed|Assignment[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getAssignments($userId)
    {
        if ($userId && $user = $this->getUser($userId)) {
            $assignment = new Assignment();
            $assignment->userId = $userId;
            $assignment->roleName = $user->getRole();
            return [$assignment->roleName => $assignment];
        }
        return [];
    }

    /**
     * @param $userId
     * @return User
     * @throws \yii\base\InvalidConfigException
     */
    private function getUser($userId): User
    {
        $webUser = Yii::$app->get('user', false);
        if ($webUser && !$webUser->getIsGuest() && $webUser->getId() == $userId) {
            return $webUser->getIdentity();
        } else {
            return User::findOne($userId);
        }
    }
}
