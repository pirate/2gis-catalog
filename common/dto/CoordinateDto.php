<?php


namespace common\dto;


/**
 * Class CoordinateDto
 * @package common\dto
 */
class CoordinateDto
{
    /**
     * @var float $longitude
     */
    public $longitude;
    /**
     * @var float $latitude
     */
    public $latitude;

    /**
     * @param float $longitude
     * @param float $latitude
     */
    public function __construct(
        float $longitude,
        float $latitude
    ) {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @return string
     */
    public function getCoordinateForGoogle(): string
    {
        return $this->getLatitude() . ',' . $this->getLongitude();
    }

    /**
     * @return array
     */
    public function getCoordinateForDb(): array
    {
        return [$this->getLatitude(), $this->getLongitude()];
    }
}
