<?php

use scl\yii\tools\components\ApiErrorHandler;


return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => '2gis-catalog',
    'bootstrap' => [
        'log',
        'common\bootstrap\SetContainer',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            'hashCallback' => function ($path) {
                $path = str_replace(Yii::getAlias('@root'), '', $path);
                return substr(hash('md4', $path), 0, 8);
            }
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                'errors' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'file' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'class' => ApiErrorHandler::class,
            'errorAction' => 'site/error',
        ],
    ],
];
