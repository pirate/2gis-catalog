<?php
return [
    'adminEmail' => 'admin@2gis.local',
    'supportEmail' => 'admin@2gis.local',
    'user.passwordResetTokenExpire' => 3600,
];
