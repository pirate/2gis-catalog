<?php

namespace common\repository;

use common\models\User;

/**
 * Class UserRepository
 *
 * @package common\repository
 */
class UserRepository
{
    /**
     * @param int|null $id
     * @return User|null
     */
    public function get(?int $id): ?User
    {
        if ($id === null) {
            return null;
        }
        return User::findOne(['id' => $id]);
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getList(array $ids): array
    {
        return User::findAll(['id' => $ids]);
    }

    /**
     * @param string $email
     *
     * @return User|null
     */
    public function getByEmail(string $email): ?User
    {
        return User::findOne(['email' => $email]);
    }
}