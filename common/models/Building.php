<?php

namespace common\models;

use common\dto\CoordinateDto;
use nanson\postgis\behaviors\GeometryBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%building}}".
 *
 * @property int $id
 * @property string $address
 * @property string|null $coordinates
 * @property string $created_at
 * @property string $updated_at
 */
class Building extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%building}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => GeometryBehavior::class,
                'attribute' => 'coordinates',
                'type' => GeometryBehavior::GEOMETRY_POINT,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['coordinates'],'each', 'rule' => ['double']],
            [['created_at', 'updated_at'], 'safe'],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'address' => Yii::t('app', 'Address'),
            'coordinates' => Yii::t('app', 'Coordinates'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return CoordinateDto
     */
    public function getCoordinate(): CoordinateDto
    {
        return new CoordinateDto(
            $this->coordinates[1],
            $this->coordinates[0]
        );
    }
}
