<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%company_section}}".
 *
 * @property int $company_id
 * @property int $section_id
 */
class CompanySection extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%company_section}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'section_id'], 'required'],
            [['company_id', 'section_id'], 'default', 'value' => null],
            [['company_id', 'section_id'], 'integer'],
            [['company_id', 'section_id'], 'unique', 'targetAttribute' => ['company_id', 'section_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'section_id' => Yii::t('app', 'Section ID'),
        ];
    }
}
