<?php

namespace backend\models\forms;

use common\models\User;
use common\repository\UserRepository;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    /** @var string $email */
    public $email;
    /** @var UserRepository $userRepository */
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required', 'message' => Yii::t('app', 'Email cannot be blank.')],
            ['email', 'email'],
        ];
    }

    /**
     * @return bool
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = $this->userRepository->getByEmail($this->email);
        if ($user === null || $user->status !== User::STATUS_ACTIVE) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject(Yii::t('app', 'Password reset for ') . Yii::$app->name)
            ->send();
    }
}
