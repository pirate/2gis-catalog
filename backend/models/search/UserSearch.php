<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends Model
{
    public $id;
    public $role;
    public $status;
    public $createdAt;
    public $updatedAt;
    public $email;
    public $phone;
    public $position;
    public $firstName;
    public $secondName;
    public $lastName;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'role', 'status', 'createdAt', 'updatedAt'], 'integer'],
            [['email', 'phone', 'firstName', 'secondName', 'lastName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'status' => $this->status,
            'created_at' => $this->createdAt,
            'updated_at' => $this->updatedAt,
        ]);

        $query->andFilterWhere(['ilike', 'email', $this->email])
            ->andFilterWhere(['ilike', 'phone', $this->phone])
            ->andFilterWhere(['ilike', 'first_name', $this->firstName])
            ->andFilterWhere(['ilike', 'second_name', $this->secondName])
            ->andFilterWhere(['ilike', 'last_name', $this->lastName]);

        return $dataProvider;
    }
}
