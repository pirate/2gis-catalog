<?php

namespace backend\assets;

use kartik\daterange\DateRangePickerAsset;
use kartik\depdrop\DepDropAsset;
use kartik\depdrop\DepDropExtAsset;
use kartik\icons\FontAwesomeAsset;
use kartik\select2\Select2Asset;
use kartik\widgets\WidgetAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;
use yii\widgets\MaskedInputAsset;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';
    /**
     * @var string
     */
    public $baseUrl = '@web';
    /**
     * @var array
     */
    public $css = [
        'css/site.css',
    ];
    /**
     * @var array
     */
    public $js = [
    ];
    /**
     * @var array
     */
    public $depends = [
            BootstrapAsset::class,
            DateRangePickerAsset::class,
            DepDropAsset::class,
            DepDropExtAsset::class,
            FontAwesomeAsset::class,
            JqueryAsset::class,
            MaskedInputAsset::class,
            Select2Asset::class,
            WidgetAsset::class,
            YiiAsset::class,
        ];
}
