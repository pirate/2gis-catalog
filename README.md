#2gis-catalog

## System requirements
- docker (download here https://www.docker.com/ or apt-get)
- docker-compose

### Prepare local DNS

Add following lines to `/etc/hosts`:

```bash
127.0.0.1 api.2gis.local admin.2gis.local
```

### Prepare docker-compose config
Copy `docker/local/project-config.yml` or `docker/dev/project-config.yml` to ``docker/project-config.yml``

#### Config details
- check system user id (call command `id` in bash) and set actual value to web and queue sections (docker/project-config.yml): `user: "1000:1000"`

### Pulling docker images
````bash
./project.sh pull
````
### Install project dependencies:
```bash
./project.sh install
```

#### Details
- ./project.sh is script for easy usage docker-compose with multiple files. Its proxy docker-compose commands and add special commands (like install/update for composer)
- Yo can see available commands: `./project.sh help`, or open file and see directly :)

### Start app for development
````bash
./project.sh up -d
````

## Настройка ENV

Для **local** версии в конфигурации подключений используем конструкции вида `$_ENV['POSTGRESL_HOST']`. Это дает гибкость
в конфигурировании приложения с помощью docker.
В настройках php обязательно разрешаем чтение env.

## Генерация swagger
Для генерации файла документации API нужно выполнить команду:

```
./project.sh swagger
```
Базовые анотации находятся в файле `api/config/bootstrap.php`

Пример использования в контроллере:
```
    /**
     * @OA\Get(
     *     tags={"auth"},
     *     path="/v1/login",
     *     summary="Get token by phone",
     *     @OA\Parameter(in="query",name="phone",required=true, @OA\Schema(type="string", example="test@test.com")),
     *
     *     @OA\Response(
     *         response="200",
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/TokenResponse")
     *     ),
     *     @OA\Response(response="500", description="Can`t create customer for unknown reason"),
     *     @OA\Response(response="422", description="This phone number is suspended")
     * )
     * @return TokenRequest|TokenResponse
     * @throws \Throwable
     * @throws SafeException
     * @throws Exception
     */
    public function actionToken()
    {
    //...
```

### message extract
`./project.sh web /app/yii message/extract backend/config/messages.php`
