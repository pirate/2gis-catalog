<?php

namespace scl\yii\tools\serializer;

use scl\tools\rest\components\BaseApiResponse;
use yii\base\Arrayable;
use yii\base\Model;
use yii\data\DataProviderInterface;
use yii\rest\Serializer;


/**
 * Class RestSerializer
 * @package scl\yii\tools\controllers
 */
class RestSerializer extends Serializer
{
    /**
     * @inheritDoc
     */
    public function serialize($data)
    {
        if ($data instanceof Model && $data->hasErrors()) {
            return $this->serializeModelErrors($data);
        } elseif ($data instanceof Arrayable) {
            return $this->serializeModel($data);
        } elseif ($data instanceof DataProviderInterface) {
            return $this->serializeDataProvider($data);
        }

        return new BaseApiResponse($data);
    }

    /**
     * Serializes the validation errors in a model.
     * @param Model $model
     * @return BaseApiResponse
     */
    protected function serializeModelErrors($model)
    {
        $this->response->setStatusCode(422, 'Data Validation Failed.');

        return new BaseApiResponse($model->getErrors(), 422, 'Data Validation Failed.', time(), false);
    }
}
