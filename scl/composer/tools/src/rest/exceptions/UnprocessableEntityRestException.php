<?php

namespace scl\tools\rest\exceptions;

use Exception;

/**
 * Class UnprocessableEntityRestException
 * @package scl\tools\rest\exceptions
 */
class UnprocessableEntityRestException extends SafeException
{
    /**
     * UnprocessableEntityRestException constructor.
     * @param null $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct(422, $message, $code, $previous);
    }
}
