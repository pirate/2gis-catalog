<?php

namespace scl\tools\rest\exceptions;

use Exception;

/**
 * Class UnauthorizedRestException
 * @package scl\tools\rest\exceptions
 */
class UnauthorizedRestException extends SafeException
{
    /**
     * UnauthorizedRestException constructor.
     * @param null $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct(401, $message, $code, $previous);
    }
}
