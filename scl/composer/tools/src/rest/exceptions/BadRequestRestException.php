<?php

namespace scl\tools\rest\exceptions;

use Exception;

/**
 * Class BadRequestRestException
 * @package scl\tools\rest\exceptions
 */
class BadRequestRestException extends SafeException
{
    /**
     * BadRequestRestException constructor.
     * @param null $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct(400, $message, $code, $previous);
    }
}
