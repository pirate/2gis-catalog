<?php

namespace scl\tools\rest\exceptions;

use Exception;

/**
 * Class NotFoundRestException
 * @package scl\tools\rest\exceptions
 */
class NotFoundRestException extends SafeException
{
    /**
     * NotFoundRestException constructor.
     * @param null $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct(404, $message, $code, $previous);
    }
}
