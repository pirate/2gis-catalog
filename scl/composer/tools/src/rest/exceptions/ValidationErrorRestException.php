<?php

namespace scl\tools\rest\exceptions;

use Exception;
use scl\tools\rest\interfaces\IRestException;

/**
 * Class ValidationErrorRestException
 * @package scl\tools\rest\exceptions
 */
class ValidationErrorRestException extends SafeException implements IRestException
{
    /**
     * @var array
     */
    private $data;

    /**
     * ValidationErrorRestException constructor.
     * @param array $errors
     * @param null $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(array $errors = [], $message = null, $code = 0, Exception $previous = null)
    {
        $this->data = $errors;
        parent::__construct(422, $message, $code, $previous);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
