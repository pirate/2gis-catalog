<?php

namespace scl\tools\rest\exceptions;

use Exception;

/**
 * Class ForbiddenRestException
 * @package scl\tools\rest\exceptions
 */
class ForbiddenRestException extends SafeException
{
    /**
     * ForbiddenRestException constructor.
     * @param null $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        parent::__construct(403, $message, $code, $previous);
    }
}
