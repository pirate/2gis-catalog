<?php

namespace scl\tools\rest\interfaces;

/**
 * Interface Exception
 * @package scl\tools\rest\interface
 */
interface IRestException
{
    /**
     * @return void
     */
    public function getData();
}
