<?php

namespace scl\tools\rest\components;

use OpenApi\Annotations as OA;
use scl\tools\rest\interfaces\IRestException;
use stdClass;
use Throwable;

/**
 * Class ErrorResponse
 * @package scl\tools\rest\components
 * @OA\Schema(schema="ErrorResponse", required={"status", "timestamp", "success"})
 */
class ErrorResponse extends AbstractApiResponse
{
    /**
     * @var boolean $success
     * @OA\Property(type="boolean", default="false", example="false")
     */
    public $success;

    /**
     * ErrorResponse constructor.
     * @param Throwable $exception
     */
    public function __construct(Throwable $exception)
    {
        if (!empty($exception->statusCode)) {
            $this->status = $exception->statusCode;
        } else {
            $this->status = $exception->getCode();
        }
        $this->message = $exception->getMessage();
        $this->timestamp = time();
        $this->success = false;
        if (is_subclass_of($exception, IRestException::class)) {
            $this->data = $exception->getData();
        } else {
            $this->data = new stdClass();
        }
    }
}
