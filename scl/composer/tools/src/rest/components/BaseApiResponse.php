<?php

namespace scl\tools\rest\components;

use OpenApi\Annotations as OA;
use stdClass;

/**
 * Class BaseApiResponse
 * @package scl\tools\rest\components
 * @OA\Schema(schema="BaseApiResponse", required={"status", "timestamp", "success"})
 */
class BaseApiResponse extends AbstractApiResponse
{
    /**
     * @var boolean $success
     * @OA\Property(type="boolean", default="true", example="true")
     */
    public $success;

    /**
     * BaseApiResponse constructor.
     * @param $response
     * @param int $status
     * @param string $message
     * @param int|null $timestamp
     * @param bool $success
     */
    public function __construct($response = null, int $status = 200, string $message = '', int $timestamp = null, bool $success = true)
    {
        $this->data = $response ?? new stdClass();
        $this->status = $status;
        $this->message = $message;
        $this->timestamp = $timestamp ?? time();
        $this->success = $success;
    }
}
