<?php

namespace scl\tools\rest\components;

use OpenApi\Annotations as OA;

/**
 * Class AbstractApiResponse
 * @package scl\tools\rest\components
 * @OA\Schema()
 */
abstract class AbstractApiResponse
{
    /**
     * @var int $status
     * @OA\Property(type="integer")
     */
    public $status;
    /**
     * @var string $message
     * @OA\Property(type="string")
     */
    public $message;
    /**
     * @var int $timestamp
     * @OA\Property(type="integer", format="int64"),
     */
    public $timestamp;
    /**
     * @var boolean $success
     * @OA\Property(type="boolean")
     */
    public $success;
    /**
     * @var mixed $data
     */
    public $data;
}
