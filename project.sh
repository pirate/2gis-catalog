#!/bin/bash

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/root
PATH=$PATH:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

ROOT=$(pwd)
CONF_DIR=$ROOT/docker
RUN=$CONF_DIR/run

MAIN=$ROOT/docker-compose.yml
CONFIG=$CONF_DIR/project-config.yml
CONNECT="-f $MAIN -f $CONFIG"

function echoYell() {
    echo -e "\033[0;33m$1\033[0m"
}
function echoBlue() {
    echo -e "\033[0;34m$1\033[0m"
}


function ShowHelp {
    echoYell 'Help'
    echoBlue 'Usage:'
    echoYell '     project.sh COMMAND [keys]'
    echoYell ''
    echoBlue 'Commands:'
    echoYell '  install|update                                  Работа с composer'
    echoYell '  down|up|stop|restart|build|pull|logs|rm|run     Работа с docker-compose проекта'
    echoYell '  remove                                          Остановить Docker сервис и удалить компоненты Docker (values, containers, networks и images)'
    echoYell '  analyze_diff                                    Проверка измененных файлов PHPStan'
    echoYell '  analyze_phpstan                                 Проверка PHPStan указанных файлов'
    echoYell '  analyze_phpmd                                   Проверка PHPMd указанных файлов'
    echoYell '  web                                             Выполнить PHP script на backend'
    echoYell '  exec                                            Подключиться к контейнеру по имени в compose'
}

function Dependencies {
    # use current dev image with fxp plugin (for bower)
    # composer cache dir
    mkdir -p $RUN/cache

    docker run --rm -v $ROOT:/app -v $RUN/cache:/composer/cache --user $(stat -c '%u' ./):$(stat -c '%g' ./) \
    gitlab.icerockdev.com:4567/docker/dev-alpine-php-7.3:latest composer $@ --prefer-dist
}

function ComposeCommand {
    docker-compose $CONNECT $@
}

function ComposeCommandExec {
    source $ROOT/.env
    if [ -t 0 ];
    then USE_TTY="";
    else USE_TTY="-T";
    fi

    ComposeCommand exec ${USE_TTY} --user $(stat -c '%u' ./):$(stat -c '%g' ./) $@
}

function run() {
    COMMAND=$1
    case "$COMMAND" in
        install|update|require)
            echo "$COMMAND  Project"
            Dependencies $@
        ;;
        remove)
            echo 'Remove Project'
            ComposeCommand down -v --rmi local
        ;;
        down|up|stop|restart|build|pull|logs|rm|run)
            ComposeCommand $@
        ;;
        web)
            shift
            echo 'Exec PHP script'
            ComposeCommandExec web $@
        ;;
        exec)
            shift
            ComposeCommandExec $@
        ;;
        swagger)
            ComposeCommandExec web php yii swagger
        ;;
        help)
            ShowHelp
        ;;
    esac
}

run $@
