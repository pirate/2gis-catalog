server {
    listen 80;
    client_max_body_size 128M;
    root /app/api/web;
    index index.php;

    location ~* \.(yml)$ {
        add_header Cache-Control 'no-store';
        etag off;
        if_modified_since off; # отключаем валидацию на nginx
        add_header Last-Modified ""; # не отправляем заголовки
        add_header Content-Type application/json;
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
    }

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    error_page 500 502 503 504 = @500.html;
    location @500.html {
        default_type application/json;
        return 503 '{"error": {"status_code": 503,"status": "Service Temporarily Unavailable"}}';
    }

    location ~ \.php$ {
        fastcgi_pass web:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PHP_VALUE "error_log=/var/log/nginx/application_php_errors.log";
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        include fastcgi_params;
    }
}
