<?php

namespace features\building\repository;

use common\dto\CoordinateDto;
use common\models\Building;
use yii\db\Expression;

/**
 * Class BuildingRepository
 * @package features\company\repository
 */
class BuildingRepository
{
    /**
     * @param int $id
     * @return Building|null
     */
    public function get(int $id): ?Building
    {
        return Building::findOne(['id' => $id]);
    }

    /**
     * @param CoordinateDto $coordinateDto
     * @param int $distance
     * @return array
     */
    public function getByRadius(
        CoordinateDto $coordinateDto,
        int $distance
    ): array
    {
        $query = new Expression("ST_Distance(ST_Transform(building.coordinates, 26986), ST_Transform(" .
            "'SRID=4326;POINT({$coordinateDto->getLongitude()} {$coordinateDto->getLatitude()})'::geometry, 26986))");
        return Building::find()->select('id')
            ->where(['<', $query, $distance])
            ->asArray()->column();
    }

    /**
     * @param CoordinateDto $firstPoint
     * @param CoordinateDto $secondPoint
     * @return array
     */
    public function getInArea(
        CoordinateDto $firstPoint,
        CoordinateDto $secondPoint
    ): array
    {
        $query = new Expression("ST_Contains(ST_Envelope('SRID=4326;LINESTRING(" .
            "{$firstPoint->getLongitude()} {$firstPoint->getLatitude()}, {$secondPoint->getLongitude()} {$secondPoint->getLatitude()}" .
            ")'::geometry), building.coordinates)");

        return Building::find()->select('id')
            ->where($query)
            ->asArray()->column();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Building[]
     */
    public function getPaginationList(int $limit, int $offset): array
    {
        return Building::find()->limit($limit)->offset($offset)->all();
    }

    /**
     * @return int
     */
    public function totalCount(): int
    {
        return (int)Building::find()->count();
    }
}