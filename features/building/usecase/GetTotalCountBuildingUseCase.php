<?php

namespace features\building\usecase;

use features\building\repository\BuildingRepository;

/**
 * Class GetTotalCountBuildingUseCase
 * @package features\office\usecase
 */
class GetTotalCountBuildingUseCase
{
    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * GetTotalCountBuildingUseCase constructor.
     * @param BuildingRepository $buildingRepository
     */
    public function __construct(
        BuildingRepository $buildingRepository
    ) {
        $this->buildingRepository = $buildingRepository;
    }

    /**
     * @return int
     */
    public function execute(): int
    {
        return $this->buildingRepository->totalCount();
    }
}
