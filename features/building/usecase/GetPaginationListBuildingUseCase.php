<?php

namespace features\building\usecase;

use common\models\Building;
use features\building\repository\BuildingRepository;

/**
 * Class GetPaginationListBuildingUseCase
 * @package features\office\usecase
 */
class GetPaginationListBuildingUseCase
{
    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * GetPaginationListBuildingUseCase constructor.
     * @param BuildingRepository $buildingRepository
     */
    public function __construct(
        BuildingRepository $buildingRepository
    ) {
        $this->buildingRepository = $buildingRepository;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Building[]
     */
    public function execute(int $limit, int $offset): array
    {
        return $this->buildingRepository->getPaginationList($limit, $offset);
    }
}
