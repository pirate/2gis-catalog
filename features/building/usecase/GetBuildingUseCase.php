<?php

namespace features\building\usecase;

use common\models\Building;
use features\building\repository\BuildingRepository;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class GetBuildingUseCase
 * @package features\office\usecase
 */
class GetBuildingUseCase
{
    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * GetBuildingUseCase constructor.
     * @param BuildingRepository $buildingRepository
     */
    public function __construct(
        BuildingRepository $buildingRepository
    ) {
        $this->buildingRepository = $buildingRepository;
    }

    /**
     * @param int $id
     * @return Building
     * @throws NotFoundHttpException
     */
    public function execute(int $id): Building
    {
        $building = $this->buildingRepository->get($id);
        if ($building === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Building not found'));
        }
        return $building;
    }
}
