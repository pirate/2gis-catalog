<?php

namespace features\section\usecase;

use features\section\repository\SectionRepository;

/**
 * Class GetSectionIdListWithChildUseCase
 * @package features\section\usecase
 */
class GetSectionIdListWithChildUseCase
{
    /**
     * @var SectionRepository
     */
    private $sectionRepository;
    /**
     * @var GetSectionUseCase
     */
    private $getSectionUseCase;

    /**
     * GetSectionIdListWithChildUseCase constructor.
     * @param SectionRepository $sectionRepository
     * @param GetSectionUseCase $getSectionUseCase
     */
    public function __construct(SectionRepository $sectionRepository, GetSectionUseCase $getSectionUseCase)
    {
        $this->sectionRepository = $sectionRepository;
        $this->getSectionUseCase = $getSectionUseCase;
    }

    /**
     * @param int $sectionId
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function execute(int $sectionId): array
    {
        $section = $this->getSectionUseCase->execute($sectionId);

        $sectionIdList = [$section->id];
        $sectionIdListForQuery = [$section->id];

        while (!empty($newSectionIdList = $this->sectionRepository->getIdListByParentId($sectionIdListForQuery))) {
            $sectionIdListForQuery = $newSectionIdList;
            $sectionIdList = array_merge($sectionIdList, $newSectionIdList);
        }
        return $sectionIdList;
    }
}