<?php

namespace features\section\usecase;

use common\models\Section;
use features\section\repository\SectionRepository;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class GetSectionUseCase
 * @package features\section\usecase
 */
class GetSectionUseCase
{
    /**
     * @var SectionRepository
     */
    private $sectionRepository;

    /**
     * GetSectionUseCase constructor.
     * @param SectionRepository $sectionRepository
     */
    public function __construct(SectionRepository $sectionRepository)
    {
        $this->sectionRepository = $sectionRepository;
    }

    /**
     * @param int $id
     * @return Section
     * @throws NotFoundHttpException
     */
    public function execute(int $id): Section
    {
        $section = $this->sectionRepository->get($id);
        if ($section === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Section not found'));
        }
        return $section;
    }
}