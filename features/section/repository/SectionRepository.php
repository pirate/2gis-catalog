<?php

namespace features\section\repository;

use common\models\Section;

/**
 * Class SectionRepository
 * @package features\company\repository
 */
class SectionRepository
{
    /**
     * @param int $id
     * @return Section|null
     */
    public function get(int $id): ?Section
    {
        return Section::findOne(['id' => $id]);
    }

    /**
     * @param array $parentIdList
     * @return array
     */
    public function getIdListByParentId(array $parentIdList): array
    {
        return Section::find()->select('id')->where(['parent_id' => $parentIdList])->column();
    }
}