<?php

namespace features\company\usecase;

use common\dto\CoordinateDto;
use features\building\repository\BuildingRepository;
use features\company\repository\CompanyRepository;

/**
 * Class ListInAreaCompanyUseCase
 * @package features\company\usecase
 */
class ListInAreaCompanyUseCase
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * ListInAreaCompanyUseCase constructor.
     * @param CompanyRepository $companyRepository
     * @param BuildingRepository $buildingRepository
     */
    public function __construct(
        CompanyRepository $companyRepository,
        BuildingRepository $buildingRepository
    ) {
        $this->companyRepository = $companyRepository;
        $this->buildingRepository = $buildingRepository;
    }

    /**
     * @param CoordinateDto $firstPoint
     * @param CoordinateDto $secondPoint
     * @return array
     */
    public function execute(CoordinateDto $firstPoint, CoordinateDto $secondPoint): array
    {
        $buildingIdList = $this->buildingRepository->getInArea($firstPoint, $secondPoint);
        return $this->companyRepository->getListByBuildingIdList($buildingIdList);
    }

}