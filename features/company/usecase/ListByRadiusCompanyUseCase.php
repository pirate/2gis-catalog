<?php

namespace features\company\usecase;

use common\dto\CoordinateDto;
use features\building\repository\BuildingRepository;
use features\company\repository\CompanyRepository;

/**
 * Class ListByRadiusCompanyUseCase
 * @package features\company\usecase
 */
class ListByRadiusCompanyUseCase
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    /**
     * @var BuildingRepository
     */
    private $buildingRepository;

    /**
     * ListBySectionCompanyUseCase constructor.
     * @param CompanyRepository $companyRepository
     * @param BuildingRepository $buildingRepository
     */
    public function __construct(
        CompanyRepository $companyRepository,
        BuildingRepository $buildingRepository
    ) {
        $this->companyRepository = $companyRepository;
        $this->buildingRepository = $buildingRepository;
    }

    /**
     * @param CoordinateDto $coordinateDto
     * @param int $radius
     * @return array
     */
    public function execute(CoordinateDto $coordinateDto, int $radius): array
    {
        $buildingIdList = $this->buildingRepository->getByRadius($coordinateDto, $radius);
        return $this->companyRepository->getListByBuildingIdList($buildingIdList);
    }

}