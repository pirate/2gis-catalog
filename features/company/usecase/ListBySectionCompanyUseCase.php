<?php

namespace features\company\usecase;

use features\company\repository\CompanyRepository;

/**
 * Class ListBySectionCompanyUseCase
 * @package features\company\usecase
 */
class ListBySectionCompanyUseCase
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * ListBySectionCompanyUseCase constructor.
     * @param CompanyRepository $companyRepository
     */
    public function __construct(
        CompanyRepository $companyRepository
    ) {
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param array $sectionIdList
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function execute(array $sectionIdList, int $limit, int $offset): array
    {
        return $this->companyRepository->getPaginationListBySectionIdList($sectionIdList, $limit, $offset);
    }

}