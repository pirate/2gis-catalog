<?php

namespace features\company\usecase;

use features\company\repository\CompanyRepository;

/**
 * Class GetTotalCountBySectionIdListCompanyUseCase
 * @package features\company\usecase
 */
class GetTotalCountBySectionIdListCompanyUseCase
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * GetTotalCountBySectionIdListCompanyUseCase constructor.
     * @param CompanyRepository $companyRepository
     */
    public function __construct(
        CompanyRepository $companyRepository
    ) {
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param array $sectionIdList
     * @return int
     */
    public function execute(array $sectionIdList): int
    {
        return $this->companyRepository->countBySectionIdList($sectionIdList);
    }

}