<?php

namespace features\company\usecase;

use features\building\usecase\GetBuildingUseCase;
use features\company\repository\CompanyRepository;
use yii\web\NotFoundHttpException;

/**
 * Class ListByBuildingCompanyUseCase
 * @package features\company\usecase
 */
class ListByBuildingCompanyUseCase
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    /**
     * @var GetBuildingUseCase
     */
    private $getBuildingUseCase;

    /**
     * ListByBuildingCompanyUseCase constructor.
     * @param CompanyRepository $companyRepository
     * @param GetBuildingUseCase $getBuildingUseCase
     */
    public function __construct(
        CompanyRepository $companyRepository,
        GetBuildingUseCase $getBuildingUseCase
    ) {
        $this->companyRepository = $companyRepository;
        $this->getBuildingUseCase = $getBuildingUseCase;
    }

    /**
     * @param int $buildingId
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws NotFoundHttpException
     */
    public function execute(int $buildingId, int $limit, int $offset): array
    {
        $building = $this->getBuildingUseCase->execute($buildingId);
        return $this->companyRepository->getPaginationListByBuildingId($building->id, $limit, $offset);
    }

}