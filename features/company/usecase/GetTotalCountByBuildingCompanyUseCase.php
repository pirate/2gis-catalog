<?php

namespace features\company\usecase;

use features\company\repository\CompanyRepository;

/**
 * Class GetTotalCountByBuildingCompanyUseCase
 * @package features\company\usecase
 */
class GetTotalCountByBuildingCompanyUseCase
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * GetTotalCountByBuildingCompanyUseCase constructor.
     * @param CompanyRepository $companyRepository
     */
    public function __construct(
        CompanyRepository $companyRepository
    ) {
        $this->companyRepository = $companyRepository;
    }

    /*
     *
     */
    public function execute(int $buildingId): int
    {
        return $this->companyRepository->countByBuilding($buildingId);
    }

}