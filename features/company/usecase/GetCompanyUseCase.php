<?php

namespace features\company\usecase;

use common\models\Company;
use features\company\repository\CompanyRepository;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class GetCompanyUseCase
 * @package features\company\usecase
 */
class GetCompanyUseCase
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * GetCompanyUseCase constructor.
     * @param CompanyRepository $companyRepository
     */
    public function __construct(
        CompanyRepository $companyRepository
    ) {
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param int $companyId
     * @return Company
     * @throws NotFoundHttpException
     */
    public function execute(int $companyId): Company
    {
        $company = $this->companyRepository->get($companyId);
        if ($company === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Company not found'));
        }
        return $company;
    }

}