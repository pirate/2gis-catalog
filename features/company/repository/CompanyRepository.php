<?php

namespace features\company\repository;

use common\models\Company;
use common\models\CompanySection;

/**
 * Class CompanyRepository
 * @package features\company\repository
 */
class CompanyRepository
{
    /**
     * @param int $id
     * @return Company|null
     */
    public function get(int $id): ?Company
    {
        return Company::findOne(['id' => $id]);
    }

    /**
     * @param int $buildingId
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPaginationListByBuildingId(int $buildingId, int $limit, int $offset): array
    {
        return Company::find()->where(['building_id' => $buildingId])->limit($limit)->offset($offset)->all();
    }

    /**
     * @param array $buildingIdList
     * @return array
     */
    public function getListByBuildingIdList(array $buildingIdList): array
    {
        return Company::find()->where(['building_id' => $buildingIdList])->all();
    }

    /**
     * @param int $buildingId
     * @return int
     */
    public function countByBuilding(int $buildingId): int
    {
        return (int)Company::find()->andWhere(['building_id' => $buildingId])->count();
    }

    /**
     * @param array $sectionIdList
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPaginationListBySectionIdList(array $sectionIdList, int $limit, int $offset): array
    {
        return Company::find()
            ->from(['c' => Company::tableName()])
            ->leftJoin(['cs' => CompanySection::tableName()],
                'c.id = cs.company_id')
            ->select('c.*')
            ->where(['cs.section_id' => $sectionIdList])->limit($limit)->offset($offset)->all();
    }

    /**
     * @param array $sectionIdList
     * @return int
     */
    public function countBySectionIdList(array $sectionIdList): int
    {
        return (int)Company::find()
            ->from(['c' => Company::tableName()])
            ->leftJoin(['cs' => CompanySection::tableName()],
            'c.id = cs.company_id')
            ->where(['cs.section_id' => $sectionIdList])
            ->count();
    }
}