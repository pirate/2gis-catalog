<?php

use yii\web\Response;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'format' => Response::FORMAT_JSON,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'identityCookie' => false,
            'enableSession' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        '/' => 'v1',
                    ],
                    'prefix' => 'v1',
                    'pluralize' => false,
                    'patterns' => [
                        'GET company/list-by-building/<buildingId:\d+>' => 'company/list-by-building',
                        'GET company/list-by-section/<sectionId:\d+>' => 'company/list-by-section',
                        'GET company/list-in-radius/' => 'company/list-in-radius',
                        'GET company/list-in-area/' => 'company/list-in-area',
                        'GET company/<id:\d+>' => 'company/view',
                        'GET building' => 'building/list'
                    ]
                ],
            ],
        ],

    ],
    'params' => $params,
];
