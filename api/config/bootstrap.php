<?php
use OpenApi\Annotations as OA;
/**
 * @OA\Info(title="2gis-catalog", version="0.1.0")
 * @OA\Server(url="95.213.235.218")
 * @OA\SecurityScheme(securityScheme="bearerAuth", type="http", scheme="bearer"),
 */

// append this!
Yii::setAlias('@root', dirname(dirname(__DIR__)));
Yii::setAlias('@storage', dirname(dirname(__DIR__)) . '/storage');
