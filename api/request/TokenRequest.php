<?php

namespace api\request;

use OpenApi\Annotations as OA;
use phpDocumentor\Reflection\Types\Integer;
use scl\yii\tools\Request;

/**
 * Class TokenRequest
 * @package api\modules\v1\request
 * @OA\Schema(required={"phone"})
 */
class TokenRequest extends Request
{
    /**
     * @var string $phone
     * @OA\Property(type="string"),
     */
    public $phone;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'integer'],
            [['phone'], 'string', 'length' => 11],
        ];
    }

    /**
     * @return int
     */
    public function getPhone(): int
    {
        return (int)$this->phone;
    }
}
