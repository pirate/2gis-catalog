<?php

namespace api\response;

use OpenApi\Annotations as OA;

/**
 * Class TokenResponse
 * @package api\modules\v1\response
 * @OA\Schema(description="Token response", required={"token"})
 */
class TokenResponse
{
    /**
     * @var string $token
     * @OA\Property(type="string")
     */
    public $token;

    /**
     * TokenResponse constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }
}
