<?php

namespace api\modules\v1\request;

use common\dto\CoordinateDto;
use scl\yii\tools\Request;

/**
 * Class CompanyListInAreaRequest
 * @package api\modules\v1\request
 */
class CompanyListInAreaRequest extends Request
{
    /**
     * @var float $firstLatitude
     */
    public $firstLatitude;
    /**
     * @var float $firstLongitude
     */
    public $firstLongitude;
    /**
     * @var float $secondLatitude
     */
    public $secondLatitude;
    /**
     * @var float $secondLongitude
     */
    public $secondLongitude;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['firstLatitude', 'firstLongitude', 'secondLatitude', 'secondLongitude'], 'required'],
            [['firstLatitude', 'firstLongitude', 'secondLatitude', 'secondLongitude'], 'double'],
        ];
    }

    /**
     * @return CoordinateDto
     */
    public function getFirstCoordinates(): CoordinateDto
    {
        return new CoordinateDto($this->firstLongitude, $this->firstLatitude);
    }

    /**
     * @return CoordinateDto
     */
    public function getSecondCoordinates(): CoordinateDto
    {
        return new CoordinateDto($this->secondLongitude, $this->secondLatitude);
    }
}
