<?php

namespace api\modules\v1\request;

use common\dto\CoordinateDto;
use scl\yii\tools\Request;

/**
 * Class CompanyListInRadiusRequest
 * @package api\modules\v1\request
 */
class CompanyListInRadiusRequest extends Request
{
    const DEFAULT_RADIUS = 100;

    /**
     * @var float $latitude
     */
    public $latitude;
    /**
     * @var float $longitude
     */
    public $longitude;
    /**
     * @var int $limit
     */
    public $radius = self::DEFAULT_RADIUS;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['latitude', 'longitude'], 'required'],
            [['radius'], 'integer', 'max' => PROJECT_MAX_INT],
            [['latitude', 'longitude'], 'double'],
        ];
    }

    /**
     * @return CoordinateDto
     */
    public function getCoordinates(): CoordinateDto
    {
        return new CoordinateDto($this->longitude, $this->latitude);
    }
}
