<?php

namespace api\modules\v1\request;

use scl\yii\tools\Request;

/**
 * Class PaginationListRequest
 * @package api\modules\v1\request
 */
class PaginationListRequest extends Request
{
    const DEFAULT_LIMIT = 100;
    const DEFAULT_OFFSET = 0;

    /**
     * @var int $limit
     */
    public $limit = self::DEFAULT_LIMIT;

    /**
     * @var int $offset
     */
    public $offset = self::DEFAULT_OFFSET;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['limit'], 'filter', 'filter' => function($value) {
                return empty($value) ? self::DEFAULT_LIMIT : $value;
            }],
            [['offset'], 'filter', 'filter' => function($value) {
                return empty($value) ? self::DEFAULT_OFFSET : $value;
            }],
            [['limit', 'offset'], 'integer', 'max' => PROJECT_MAX_INT],
        ];
    }
}
