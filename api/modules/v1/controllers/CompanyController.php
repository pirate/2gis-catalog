<?php

namespace api\modules\v1\controllers;

use api\modules\v1\request\CompanyListInAreaRequest;
use api\modules\v1\request\CompanyListInRadiusRequest;
use api\modules\v1\response\CompanyListResponse;
use api\modules\v1\request\PaginationListRequest;
use api\modules\v1\response\CompanyResponse;
use features\company\usecase\GetCompanyUseCase;
use features\company\usecase\GetTotalCountByBuildingCompanyUseCase;
use features\company\usecase\GetTotalCountBySectionIdListCompanyUseCase;
use features\company\usecase\ListInAreaCompanyUseCase;
use features\company\usecase\ListByBuildingCompanyUseCase;
use features\company\usecase\ListByRadiusCompanyUseCase;
use features\company\usecase\ListBySectionCompanyUseCase;
use features\section\usecase\GetSectionIdListWithChildUseCase;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\web\NotFoundHttpException;

/**
 * Class CompanyController
 * @package api\modules\v1\controllers
 *
 * @OA\Schema(
 *      schema="CompanyListResponseWrap",
 *      type="object",
 *      allOf={
 *          @OA\Schema(ref="#/components/schemas/BaseApiResponse"),
 *          @OA\Schema(
 *              required={"data"},
 *              @OA\Property(property="data", ref="#/components/schemas/CompanyListResponse")
 *          )
 *      }
 * )
 *
 */
class CompanyController extends DefaultController
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'actions' => [
                        'list-by-building',
                        'list-by-section',
                        'list-in-radius',
                        'list-in-area',
                        'view',
                    ],
                    'roles' => ['?'],
                ],
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'except' => [
                'list-by-building',
                'list-by-section',
                'list-in-radius',
                'list-in-area',
                'view',
            ]
        ];
        $behaviors['verbFilter']['actions'] = [
            'list-by-building' => ['GET'],
            'list-by-section' => ['GET'],
            'list-in-radius' => ['GET'],
            'list-in-area' => ['GET'],
            'view' => ['GET'],
        ];
        return $behaviors;
    }

    /**
     * @OA\Get(
     *     tags={"company"},
     *     path="/v1/company/list-by-building",
     *     summary="Company list by building id",
     *
     *     @OA\Parameter(in="path",name="buildingId", @OA\Schema(type="integer", example=1), required=true),
     *     @OA\Parameter(in="query",name="limit", @OA\Schema(type="integer", example=1, default=100)),
     *     @OA\Parameter(in="query",name="offset", @OA\Schema(type="integer", example=1, default=0)),
     *     @OA\Response(
     *         response="200",
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/CompanyListResponseWrap"),
     *     ),
     *     @OA\Response(response="404", description="This building not found",
     *          @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *    )
     * )
     *
     * @param ListByBuildingCompanyUseCase $listByBuildingCompanyUseCase
     * @param GetTotalCountByBuildingCompanyUseCase $getTotalCountByBuildingCompanyUseCase
     * @param $buildingId
     * @return CompanyListResponse|PaginationListRequest
     * @throws NotFoundHttpException
     */
    public function actionListByBuilding(
        ListByBuildingCompanyUseCase $listByBuildingCompanyUseCase,
        GetTotalCountByBuildingCompanyUseCase $getTotalCountByBuildingCompanyUseCase,
        $buildingId
    ) {
        $request = new PaginationListRequest($this->input);
        if (!$request->validate()) {
            return $request;
        }

        $companyList = $listByBuildingCompanyUseCase->execute($buildingId, $request->limit, $request->offset);
        $totalCount = $getTotalCountByBuildingCompanyUseCase->execute($buildingId);

        return new CompanyListResponse($companyList, $totalCount);
    }

    /**
     * @OA\Get(
     *     tags={"company"},
     *     path="/v1/company/list-by-section",
     *     summary="Company list by section id",
     *
     *     @OA\Parameter(in="path",name="sectionId", @OA\Schema(type="integer", example=1), required=true),
     *     @OA\Parameter(in="query",name="limit", @OA\Schema(type="integer", example=1, default=100)),
     *     @OA\Parameter(in="query",name="offset", @OA\Schema(type="integer", example=1, default=0)),
     *     @OA\Response(
     *         response="200",
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/CompanyListResponseWrap"),
     *     ),
     *     @OA\Response(response="404", description="This section not found",
     *          @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *    ),
     * )
     *
     * @param GetSectionIdListWithChildUseCase $getSectionIdListWithChildUseCase
     * @param ListBySectionCompanyUseCase $listBySectionCompanyUseCase
     * @param GetTotalCountBySectionIdListCompanyUseCase $getTotalCountBySectionIdListCompanyUseCase
     * @param $sectionId
     * @return CompanyListResponse|PaginationListRequest
     * @throws NotFoundHttpException
     */
    public function actionListBySection(
        GetSectionIdListWithChildUseCase $getSectionIdListWithChildUseCase,
        ListBySectionCompanyUseCase $listBySectionCompanyUseCase,
        GetTotalCountBySectionIdListCompanyUseCase $getTotalCountBySectionIdListCompanyUseCase,
        $sectionId
    ) {
        $request = new PaginationListRequest($this->input);
        if (!$request->validate()) {
            return $request;
        }

        $sectionIdList = $getSectionIdListWithChildUseCase->execute($sectionId);
        $companyList = $listBySectionCompanyUseCase->execute($sectionIdList, $request->limit, $request->offset);
        $totalCount = $getTotalCountBySectionIdListCompanyUseCase->execute($sectionIdList);

        return new CompanyListResponse($companyList, $totalCount);
    }

    /**
     * @OA\Get(
     *     tags={"company"},
     *     path="/v1/company/list-in-radius",
     *     summary="Company list in radius",
     *
     *     @OA\Parameter(in="query",name="latitude", @OA\Schema(type="number", example=82.919569), required=true),
     *     @OA\Parameter(in="query",name="longitude", @OA\Schema(type="integer", example=55.031137), required=true),
     *     @OA\Parameter(in="query",name="radius", @OA\Schema(type="integer", description="radius for search in meters", example=1, default=100)),
     *     @OA\Response(
     *         response="200",
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/CompanyListResponseWrap"),
     *     ),
     * )
     *
     * @param ListByRadiusCompanyUseCase $listByRadiusCompanyUseCase
     * @return CompanyListInRadiusRequest|CompanyListResponse
     */
    public function actionListInRadius(
        ListByRadiusCompanyUseCase $listByRadiusCompanyUseCase
    ) {
        $request = new CompanyListInRadiusRequest($this->input);
        if (!$request->validate()) {
            return $request;
        }

        $companyList = $listByRadiusCompanyUseCase->execute($request->getCoordinates(), $request->radius);

        return new CompanyListResponse($companyList, count($companyList));
    }

    /**
     * @OA\Get(
     *     tags={"company"},
     *     path="/v1/company/list-in-area",
     *     summary="Company list in specific area, need specify two points",
     *
     *     @OA\Parameter(in="query",name="firstLatitude", @OA\Schema(type="number", example=82.919569), required=true),
     *     @OA\Parameter(in="query",name="firstLongitude", @OA\Schema(type="integer", example=55.031137), required=true),
     *     @OA\Parameter(in="query",name="secondLatitude", @OA\Schema(type="number", example=82.919569), required=true),
     *     @OA\Parameter(in="query",name="secondLongitude", @OA\Schema(type="integer", example=55.031137), required=true),
     *     @OA\Response(
     *         response="200",
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/CompanyListResponseWrap"),
     *     ),
     * )
     *
     * @param ListInAreaCompanyUseCase $listInAreaCompanyUseCase
     * @return CompanyListInAreaRequest|CompanyListResponse
     */
    public function actionListInArea(
        ListInAreaCompanyUseCase $listInAreaCompanyUseCase
    ) {
        $request = new CompanyListInAreaRequest($this->input);
        if (!$request->validate()) {
            return $request;
        }

        $companyList = $listInAreaCompanyUseCase->execute($request->getFirstCoordinates(), $request->getSecondCoordinates());

        return new CompanyListResponse($companyList, count($companyList));
    }

    /**
     * @OA\Schema(
     *      schema="CompanyResponseWrap",
     *      type="object",
     *      allOf={
     *          @OA\Schema(ref="#/components/schemas/BaseApiResponse"),
     *          @OA\Schema(
     *              required={"data"},
     *              @OA\Property(property="data", ref="#/components/schemas/CompanyResponse")
     *          )
     *      }
     * )
     *
     * @OA\Get(
     *     tags={"company"},
     *     path="/v1/company/{id}",
     *     summary="Company detail view by id",
     *
     *     @OA\Parameter(in="path",name="id", @OA\Schema(type="integer", example=1), required=true),
     *     @OA\Response(
     *         response="200",
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/CompanyResponseWrap"),
     *     ),
     *     @OA\Response(response="404", description="This company not found",
     *          @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *    ),
     * )
     *
     * @param GetCompanyUseCase $getCompanyUseCase
     * @param $id
     * @return CompanyResponse
     * @throws NotFoundHttpException
     */
    public function actionView(
        GetCompanyUseCase $getCompanyUseCase,
        $id
    ) {
        $company = $getCompanyUseCase->execute($id);

        return new CompanyResponse($company);
    }
}