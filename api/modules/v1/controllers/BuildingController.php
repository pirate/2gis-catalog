<?php

namespace api\modules\v1\controllers;

use api\modules\v1\response\BuildingListResponse;
use api\modules\v1\request\PaginationListRequest;
use features\building\usecase\GetPaginationListBuildingUseCase;
use features\building\usecase\GetTotalCountBuildingUseCase;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;

/**
 * Class BuildingController
 * @package api\modules\v1\controllers
 *
 * @OA\Schema(
 *      schema="BuildingListResponseWrap",
 *      type="object",
 *      allOf={
 *          @OA\Schema(ref="#/components/schemas/BaseApiResponse"),
 *          @OA\Schema(
 *              required={"data"},
 *              @OA\Property(property="data", ref="#/components/schemas/BuildingListResponse")
 *          )
 *      }
 * )
 *
 */
class BuildingController extends DefaultController
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => true,
                    'actions' => [
                        'list',
                    ],
                    'roles' => ['?'],
                ],
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'except' => [
                'list',
            ]
        ];
        $behaviors['verbFilter']['actions'] = [
            'list' => ['GET'],
        ];
        return $behaviors;
    }

    /**
     * @OA\Get(
     *     tags={"Building"},
     *     path="/v1/building",
     *     summary="Building list",
     *
     *     @OA\Parameter(in="query",name="limit", @OA\Schema(type="integer", example=1, default=100)),
     *     @OA\Parameter(in="query",name="offset", @OA\Schema(type="integer", example=1, default=0)),
     *     @OA\Response(
     *         response="200",
     *         description="Ok",
     *         @OA\JsonContent(ref="#/components/schemas/CompanyListResponseWrap"),
     *     ),
     * )
     *
     * @param GetPaginationListBuildingUseCase $getPaginationListBuildingUseCase
     * @param GetTotalCountBuildingUseCase $getTotalCountBuildingUseCase
     * @return BuildingListResponse|PaginationListRequest
     */
    public function actionList(
        GetPaginationListBuildingUseCase $getPaginationListBuildingUseCase,
        GetTotalCountBuildingUseCase $getTotalCountBuildingUseCase
    ) {
        $request = new PaginationListRequest($this->input);
        if (!$request->validate()) {
            return $request;
        }

        $buildingList = $getPaginationListBuildingUseCase->execute($request->limit, $request->offset);
        $totalCount = $getTotalCountBuildingUseCase->execute();

        return new BuildingListResponse($buildingList, $totalCount);
    }
}