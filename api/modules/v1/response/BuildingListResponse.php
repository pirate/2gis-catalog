<?php

namespace api\modules\v1\response;

use common\models\Building;
use OpenApi\Annotations as OA;

/**
 * Class BuildingListResponse
 * @package api\modules\v1\response
 * @OA\Schema(description="Building list response", required={"officeList", "totalCount"})
 */
class BuildingListResponse
{
    /**
     * @var array $buildingList
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/BuildingResponse"))
     */
    public $buildingList = [];
    /**
     * @var int $totalCount
     * @OA\Property(type="integer")
     */
    public $totalCount;

    /**
     * BuildingListResponse constructor.
     * @param Building[] $buildingList
     * @param int $totalCount
     */
    public function __construct(array $buildingList, int $totalCount)
    {
        $this->totalCount = $totalCount;
        
        foreach ($buildingList as $building) {
            $this->buildingList[] = new BuildingResponse($building);
        }
    }
}
