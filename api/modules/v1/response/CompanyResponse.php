<?php

namespace api\modules\v1\response;

use common\models\Company;
use OpenApi\Annotations as OA;

/**
 * Class CompanyResponse
 * @package api\modules\v1\response
 * @OA\Schema(description="Company", required={"id", "name", "buildingId"})
 */
class CompanyResponse
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    public $id;
    /**
     * @var string $name
     * @OA\Property(type="string")
     */
    public $name;
    /**
     * @var array $phoneList
     * @OA\Property(type="array", @OA\Items(type="string"))
     */
    public $phoneList;
    /**
     * @var int $buildingId
     * @OA\Property(type="integer")
     */
    public $buildingId;

    /**
     * CompanyResponse constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->id = $company->id;
        $this->name = $company->name;
        $this->phoneList = $company->phone;
        $this->buildingId = $company->building_id;
    }
}
