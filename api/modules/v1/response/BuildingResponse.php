<?php

namespace api\modules\v1\response;

use common\models\Building;
use OpenApi\Annotations as OA;

/**
 * Class BuildingResponse
 * @package api\modules\v1\response
 * @OA\Schema(description="Building", required={"id", "address", "coordinates"})
 */
class BuildingResponse
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    public $id;
    /**
     * @var string $address
     * @OA\Property(type="string")
     */
    public $address;
    /**
     * @var array $coordinates
     * @OA\Property(ref="#/components/schemas/CoordinateResponse")
     */
    public $coordinates;

    /**
     * BuildingResponse constructor.
     * @param Building $building
     */
    public function __construct(Building $building)
    {
        $this->id = $building->id;
        $this->address = $building->address;
        $this->coordinates = new CoordinateResponse($building->getCoordinate());
    }
}
