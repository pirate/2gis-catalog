<?php

namespace api\modules\v1\response;

use common\models\Company;
use OpenApi\Annotations as OA;

/**
 * Class CompanyListResponse
 * @package api\modules\v1\response
 * @OA\Schema(description="Company list response", required={"officeList", "totalCount"})
 */
class CompanyListResponse
{
    /**
     * @var array $companyList
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/CompanyResponse"))
     */
    public $companyList = [];
    /**
     * @var int $totalCount
     * @OA\Property(type="integer")
     */
    public $totalCount;

    /**
     * CompanyListResponse constructor.
     * @param Company[] $companyList
     * @param int $totalCount
     */
    public function __construct(array $companyList, int $totalCount)
    {
        $this->totalCount = $totalCount;
        
        foreach ($companyList as $company) {
            $this->companyList[] = new CompanyResponse($company);
        }
    }
}
