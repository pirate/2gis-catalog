<?php

namespace api\modules\v1\response;

use common\dto\CoordinateDto;
use OpenApi\Annotations as OA;

/**
 * Class CoordinateResponse
 * @package api\modules\v1\response
 * @OA\Schema(description="Coordinates", required={"longitude", "latitude"})
 */
class CoordinateResponse
{
    /**
     * @var float $longitude
     * @OA\Property(type="number")
     */
    public $longitude;
    /**
     * @var float $latitude
     * @OA\Property(type="number")
     */
    public $latitude;

    /**
     * CoordinateResponse constructor.
     * @param CoordinateDto $coordinateDto
     */
    public function __construct(CoordinateDto $coordinateDto)
    {
        $this->longitude = $coordinateDto->getLongitude();
        $this->latitude = $coordinateDto->getLatitude();
    }
}
