<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m200118_030321_insert_data
 */
class m200118_030321_insert_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $buildingColumns = ['id', 'address', 'coordinates'];
        $buildingValues = [
            [1, 'Красный проспект 29', new Expression('ST_SetSRID(ST_MakePoint(82.919569, 55.031137), 4326)')],
            [2, 'Красный проспект 31', new Expression('ST_SetSRID(ST_MakePoint(82.919054, 55.032016), 4326)')],
            [3, 'Красный проспект 35', new Expression('ST_SetSRID(ST_MakePoint(82.918829, 55.034236), 4326)')],
            [4, 'Красный проспект 55', new Expression('ST_SetSRID(ST_MakePoint(82.917229, 55.039315), 4326)')],
            [5, 'Красный проспект 75', new Expression('ST_SetSRID(ST_MakePoint(82.915029, 55.047013), 4326)')],
            [6, 'Красный проспект 81', new Expression('ST_SetSRID(ST_MakePoint(82.913940, 55.051450), 4326)')],
            [7, 'Красный проспект 87', new Expression('ST_SetSRID(ST_MakePoint(82.913158, 55.054269), 4326)')],
            [8, 'Фрунзе 232', new Expression('ST_SetSRID(ST_MakePoint(82.955448, 55.039343), 4326)')],
            [9, 'Никитина 149', new Expression('ST_SetSRID(ST_MakePoint(82.983493, 55.028257), 4326)')],
            [10, 'Никитина 152', new Expression('ST_SetSRID(ST_MakePoint(82.919569, 55.031137), 4326)')],
        ];

        Yii::$app->db->createCommand()->batchInsert('{{%building}}', $buildingColumns,
            $buildingValues)->execute();

        $companyColumns = ['id', 'name', 'phone', 'building_id'];
        $companyValues = [
            [1, 'Company', new Expression('ARRAY[79131111111, 79232222222]::text[]'), 1],
            [2, 'Company1', new Expression('ARRAY[79131111111, 79232222222]::text[]'), 1],
            [3, 'Company2', new Expression('ARRAY[79134441111, 79235552222]::text[]'), 2],
            [4, 'Company3', new Expression('ARRAY[79131112222, 79232222333]::text[]'), 2],
            [5, 'Company4', new Expression('ARRAY[79131166111, 79232211222]::text[]'), 2],
            [6, 'Company5', new Expression('ARRAY[79131166611, 79232111222, 79232115522]::text[]'), 3],
            [7, 'Company6', new Expression('ARRAY[79131777111, 79232244222]::text[]'), 4],
            [8, 'Company7', new Expression('ARRAY[79131155511, 79232244422]::text[]'), 4],
            [9, 'Company8', new Expression('ARRAY[79131111111, 79232221122]::text[]'), 5],
            [10, 'Company9', new Expression('ARRAY[79131441111, 7923232222]::text[]'), 5],
            [11, 'Company11', new Expression('ARRAY[79138881111, 79232245222]::text[]'), 5],
            [12, 'Company12', new Expression('ARRAY[79131111111, 79232266222]::text[]'), 5],
            [13, 'Company13', new Expression('ARRAY[79131119991, 79234565222]::text[]'), 6],
            [14, 'Company14', new Expression('ARRAY[79131222111, 79232554422]::text[]'), 6],
            [15, 'Company15', new Expression('ARRAY[79131143411, 79232266222]::text[]'), 7],
            [16, 'Company16', new Expression('ARRAY[79131545111, 79232266622]::text[]'), 7],
            [17, 'Company17', new Expression('ARRAY[79131661111, 79232227772]::text[]'), 8],
            [18, 'Company18', new Expression('ARRAY[79131175751, 79232244222]::text[]'), 8],
            [19, 'Company19', new Expression('ARRAY[79131656111, 79232657522]::text[]'), 9],
            [20, 'Company21', new Expression('ARRAY[79131341111, 79232345422]::text[]'), 9],
            [21, 'Company22', new Expression('ARRAY[79131545411, 79232244222]::text[]'), 9],
            [22, 'Company23', new Expression('ARRAY[79131176761, 79232277722]::text[]'), 10],
        ];

        Yii::$app->db->createCommand()->batchInsert('{{%company}}', $companyColumns,
            $companyValues)->execute();

        $sectionColumns = ['id', 'name', 'parent_id'];
        $sectionValues = [
            [1, 'Еда', null],
            [2, 'Полуфабрикаты', 1],
            [3, 'Мясная продукция', 1],
            [4, 'Автомобили', null],
            [5, 'Грузовые', 4],
            [6, 'Легковые', 4],
            [7, 'Запчасти', 6],
            [8, 'Шины/Диски', 6],
            [9, 'Запчасти для подвески', 7],
            [10, 'Спорт', null],
            [11, 'Тренажеры', 10],
        ];

        Yii::$app->db->createCommand()->batchInsert('{{%section}}', $sectionColumns,
            $sectionValues)->execute();

        $columns = ['company_id', 'section_id'];
        $values = [
            [1, 1],
            [2, 9],
            [3, 8],
            [4, 2],
            [5, 3],
            [6, 4],
            [7, 5],
            [8, 6],
            [9, 7],
            [10, 8],
            [11, 9],
            [12, 10],
            [13, 11],
            [14, 1],
            [15, 2],
            [16, 4],
            [17, 5],
            [18, 6],
            [19, 7],
            [20, 11],
            [21, 7],
        ];

        Yii::$app->db->createCommand()->batchInsert('{{%company_section}}', $columns,
            $values)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('{{%company_section}}');
        $this->truncateTable('{{%section}}');
        $this->truncateTable('{{%company}}');
        $this->truncateTable('{{%building}}');
    }
}
