<?php

use yii\db\Migration;
use \common\models\User;

/**
 * Class m181113_083314_add_super_admin_user
 */
class m181113_083314_add_super_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'email' => 'admin@2gis.local',
            'password_hash' => Yii::$app->security->generatePasswordHash('123123'), // default password is 123123
            'auth_key' => Yii::$app->security->generateRandomString(),
            'phone' => 77777777777,
            'first_name' => 'Администратор',
            'second_name' => 'Администратор',
            'last_name' => 'Администратор',
            'role' => User::ROLE_SERVICE_OWNER,
            'status' => User::STATUS_ACTIVE,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['email' => 'admin@2gis.local']);
    }
}
