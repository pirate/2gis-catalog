<?php

use yii\db\Expression;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'phone' => $this->bigInteger()->notNull()->unique(),
            'first_name' => $this->string()->notNull(),
            'second_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'role' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->createTable('{{%company}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'phone' => 'text[]',
            'building_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
            'updated_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
        ]);

        $this->createTable('{{%building}}', [
            'id' => $this->primaryKey(),
            'address' => $this->string()->notNull(),
            'coordinates' => 'geometry(POINT)',
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
            'updated_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
        ]);

        $this->createTable('{{%section}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'parent_id' => $this->integer()->null(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
            'updated_at' => $this->timestamp()->defaultExpression('NOW()')->notNull(),
        ]);

        $this->createTable('{{%company_section}}', [
            'company_id' => $this->integer()->null(),
            'section_id' => $this->integer()->notNull(),
            'PRIMARY KEY(company_id, section_id)',
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%company}}');
        $this->dropTable('{{%building}}');
        $this->dropTable('{{%section}}');
        $this->dropTable('{{%company_section}}');

    }
}
