<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use function OpenApi\scan;

/**
 * Class SwaggerController
 *
 * @package console\controllers
 */
class SwaggerController extends Controller
{
    /**
     * Special control for swagger generate
     */
    public function actionIndex()
    {
        ini_set('memory_limit', '1000M');
        $openApi = scan(Yii::getAlias('@root'), ['exclude' => ['vendor', 'docker', 'console']]);
        $data = $openApi->toYaml();

        $fileName = Yii::getAlias('@api') . '/web/swagger.yml';
        file_put_contents($fileName, $data);

        echo "Success!\n";
    }
}
